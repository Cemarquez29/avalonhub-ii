package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Especialidad;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;
import co.edu.uniquindio.avalon.repositorios.EspecialidadRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EspecialidadServicioImpl implements EspecialidadServicio{

    @Autowired
    private EspecialidadRepo especialidadRepo;

    @Override
    public Especialidad registrarEspecialidad(Especialidad especialidad) throws AlreadyExistException {
        Optional<Especialidad> buscado = especialidadRepo.findById(especialidad.getCodigo());
        if(buscado.isPresent()){
            throw new AlreadyExistException("Ya existe la especialidad");
        }
        return especialidadRepo.save(especialidad);
    }

    @Override
    public void eliminarEspecialidad(Especialidad especialidad) throws NotExistException {
        Optional<Especialidad> buscado = especialidadRepo.findById(especialidad.getCodigo());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe la especialidad ");
        }
        especialidadRepo.delete(especialidad);
    }

    @Override
    public Especialidad actualizarEspecialidad(Especialidad especialidad) throws NotExistException {
        Optional<Especialidad> buscado = especialidadRepo.findById(especialidad.getCodigo());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe la especialidad que va a actualizar");
        }
        return especialidadRepo.save(especialidad);
    }

    @Override
    public List<Especialidad> listarEspecialidades() {
        return especialidadRepo.findAll();
    }
}
