package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Horario;
import co.edu.uniquindio.avalon.entidades.Clase;
import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;

import java.util.List;

public interface UsuarioServicio {
    Usuario registrarUsuario(Usuario usuario) throws AlreadyExistException;
    void eliminarUsuario(Usuario usuario) throws NotExistException;
    Usuario actualizarUsuario(Usuario usuario) throws  NotExistException;
    Usuario iniciarSesion(String usuario, String contrasena) throws NotExistException;
    Usuario recuperarContrasenia(String usuario) throws  NotExistException;
    List<Usuario> listarUsuarios();
    List<Recarga> listarRecargas(String usuario);
    List<Clase> listarClases(String usuario);
    List<Clase> listarClasesCanceladas(String usuario);
    List<Horario> listarHorario(String usuario);
    Usuario obtenerUsuario(String usuario);


}
