package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;
import co.edu.uniquindio.avalon.repositorios.RecargaRepo;
import co.edu.uniquindio.avalon.repositorios.UsuarioRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecargaServicioImpl implements RecargaServicio {

    @Autowired
    private RecargaRepo recargaRepo;

    @Autowired
    private UsuarioRepo usuarioRepo;

    @Override
    public Recarga registrarRecarga(Recarga recarga, Usuario usuario) {
        usuario.setSaldo(usuario.getSaldo() + recarga.getValor());
        usuarioRepo.save(usuario);
        return recargaRepo.save(recarga);
    }

    @Override
    public int obtenerCodigoSiquiente() {
        return recargaRepo.obtenerCodigoSiguiente().get(0);
    }
}
