package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Categoria;
import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.repositorios.CategoriaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriaServicioImpl implements CategoriaServicio {

    @Autowired
    private CategoriaRepo categoriaRepo;

    @Override
    public Categoria registrarCategoria(Categoria categoria) throws AlreadyExistException {
        Optional<Categoria> guardado = categoriaRepo.findById(categoria.getCodigo());

        if(guardado.isPresent())
            throw new AlreadyExistException("Ya existe esta categoria");

        return categoriaRepo.save(categoria);
    }


    @Override
    public List<Categoria> listaCategorias() {
        return categoriaRepo.findAll();
    }

    @Override
    public Categoria obtenerCategoria(int codigo) {
        Optional<Categoria> categoriaOptional = categoriaRepo.findById(codigo);
        if(categoriaOptional.isPresent())
            return categoriaOptional.get();

        return null;
    }
}
