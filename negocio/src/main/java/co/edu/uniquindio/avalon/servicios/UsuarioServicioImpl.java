package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Clase;
import co.edu.uniquindio.avalon.entidades.Horario;
import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;
import co.edu.uniquindio.avalon.repositorios.UsuarioRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServicioImpl implements UsuarioServicio{

    @Autowired
    private UsuarioRepo usuarioRepo;

    @Override
    public Usuario registrarUsuario(Usuario usuario) throws AlreadyExistException {

        Optional<Usuario> buscado = usuarioRepo.findById(usuario.getUsername());
        if(buscado.isPresent()){
            throw new AlreadyExistException("Ya existe el usuario");
        }

        return usuarioRepo.save(usuario);

    }

    @Override
    public void eliminarUsuario(Usuario usuario) throws NotExistException {
        Optional<Usuario> buscado = usuarioRepo.findById(usuario.getUsername());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe el usuario");
        }
        usuarioRepo.delete(usuario);
    }

    @Override
    public Usuario actualizarUsuario(Usuario usuario) throws NotExistException {
        Optional<Usuario> buscado = usuarioRepo.findById(usuario.getUsername());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe el usuario a actualizar");
        }
        return usuarioRepo.save(usuario);
    }

    @Override
    public Usuario iniciarSesion(String usuario, String contrasena) throws NotExistException {
        Usuario u = usuarioRepo.findByUsernameAndContrasena(usuario, contrasena);
        if (u == null)
        {
            throw new NotExistException("Usuario o contraseña incorrectos");
        }
        return u;
    }

    @Override
    public Usuario recuperarContrasenia(String usuario) throws NotExistException {

        Usuario user = usuarioRepo.findByCorreo(usuario);

        if (user!=null){
            return user;
        } else {

            user = usuarioRepo.findByUsername(usuario);
        }
        if(user == null)
        {
            throw new NotExistException("No existe el usuario ingresado");
        }
        return  user;
    }

    @Override
    public List<Usuario> listarUsuarios() {
        return usuarioRepo.findAll();
    }

    @Override
    public List<Recarga> listarRecargas(String usuario) {
        return usuarioRepo.listarRecargas(usuario);
    }

    @Override
    public List<Clase> listarClases(String usuario) {
        return usuarioRepo.listarClases(usuario);
    }

    @Override
    public List<Clase> listarClasesCanceladas(String usuario) {
        return usuarioRepo.listarClasesCanceladas(usuario);
    }

    @Override
    public List<Horario> listarHorario(String usuario) {
        return usuarioRepo.listarHorarios(usuario);
    }

    @Override
    public Usuario obtenerUsuario(String usuario) {
        Optional<Usuario> usuarioOptional = usuarioRepo.findById(usuario);
        if( usuarioOptional.isPresent())
            return usuarioOptional.get();

        return null;
    }
}
