package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Clase;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;

import java.util.List;

public interface ClaseServicio {

    Clase registrarClase(Clase clase) throws AlreadyExistException;
    void eliminarClase(Clase clase) throws NotExistException;
    Clase actualizarClase(Clase clase) throws  NotExistException;
    List<Clase> listarClase();


}
