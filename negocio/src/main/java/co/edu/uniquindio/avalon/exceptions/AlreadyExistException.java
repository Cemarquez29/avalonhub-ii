package co.edu.uniquindio.avalon.exceptions;

public class AlreadyExistException extends Exception{

    public AlreadyExistException(String message) {
        super(message);
    }
}
