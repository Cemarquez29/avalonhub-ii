package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.entidades.Especialidad;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;
import co.edu.uniquindio.avalon.repositorios.CoachRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CoachServicioImpl implements CoachServicio{

    @Autowired
    private CoachRepo coachRepo;

    @Override
    public Coach registrarProfesor(Coach coach) throws AlreadyExistException {
        Optional<Coach> buscado = coachRepo.findById(coach.getCedula());
        if(buscado.isPresent()){
            throw new AlreadyExistException("Ya existe el coach");
        }
        return coachRepo.save(coach);
    }

    @Override
    public void eliminarProfesor(Coach coach) throws NotExistException {
        Optional<Coach> buscado = coachRepo.findById(coach.getCedula());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe el coach");
        }
        coachRepo.delete(coach);
    }

    @Override
    public Coach actualizarProfesor(Coach coach) throws NotExistException {
        Optional<Coach> buscado = coachRepo.findById(coach.getCedula());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe el coach que va a actualizar");
        }
        return coachRepo.save(coach);
    }

    @Override
    public List<Coach> listarProfesores() {
        return coachRepo.findAll();
    }

    @Override
    public List<Coach> listarProfesoresPorJuego(int juego) {
        return coachRepo.listarProfesoresPorJuego(juego);
    }

    @Override
    public Especialidad especialidadPorJuego(int juego, String profesor) {
        return coachRepo.EspecialidadPorJuego(juego, profesor);
    }

    @Override
    public Coach obtenerCoach(String coach) {
        Optional<Coach> coachOptional = coachRepo.findById(coach);
        if(coachOptional.isPresent())
            return coachOptional.get();

        return null;
    }
}
