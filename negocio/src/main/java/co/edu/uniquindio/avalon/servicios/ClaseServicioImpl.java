package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Clase;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;
import co.edu.uniquindio.avalon.repositorios.ClaseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClaseServicioImpl implements ClaseServicio {

    @Autowired
    private ClaseRepo claseRepo;
    @Override
    public Clase registrarClase(Clase clase) throws AlreadyExistException {
        Optional<Clase> buscado = claseRepo.findById(clase.getCodigo());
        if(buscado.isPresent()){
            throw new AlreadyExistException("Ya existe la clase");
        }

        return claseRepo.save(clase);
    }

    @Override
    public void eliminarClase(Clase clase) throws NotExistException {
        Optional<Clase> buscado = claseRepo.findById(clase.getCodigo());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe la clase");
        }
        claseRepo.delete(clase);
    }

    @Override
    public Clase actualizarClase(Clase clase) throws NotExistException {
        Optional<Clase> buscado = claseRepo.findById(clase.getCodigo());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe la clase que va a actualizar");
        }
        return claseRepo.save(clase);
    }

    @Override
    public List<Clase> listarClase() {
        return claseRepo.findAll();
    }
}
