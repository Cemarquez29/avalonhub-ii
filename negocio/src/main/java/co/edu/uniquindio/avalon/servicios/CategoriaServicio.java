package co.edu.uniquindio.avalon.servicios;

    import co.edu.uniquindio.avalon.entidades.Categoria;
    import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
    import java.util.List;

    public interface CategoriaServicio{

        Categoria registrarCategoria(Categoria categoria) throws AlreadyExistException;
        List<Categoria> listaCategorias();
        Categoria obtenerCategoria(int codigo);

}
