package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Especialidad;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;

import java.util.List;

public interface EspecialidadServicio {

    Especialidad registrarEspecialidad(Especialidad especialidad) throws AlreadyExistException;
    void eliminarEspecialidad(Especialidad especialidad) throws NotExistException;
    Especialidad actualizarEspecialidad(Especialidad especialidad) throws  NotExistException;
    List<Especialidad> listarEspecialidades();

}
