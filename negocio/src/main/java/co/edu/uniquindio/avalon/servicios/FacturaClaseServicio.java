package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.FacturaClase;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;

import java.util.List;

public interface FacturaClaseServicio {

    FacturaClase registrarFacturaClase(FacturaClase facturaClase) throws AlreadyExistException;
    List<FacturaClase> listarFacturas();

}
