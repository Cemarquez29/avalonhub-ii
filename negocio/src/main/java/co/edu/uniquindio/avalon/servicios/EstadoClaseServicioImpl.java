package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.EstadoClase;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.repositorios.EstadoClaseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoClaseServicioImpl implements EstadoClaseServicio {

    @Autowired
    private EstadoClaseRepo estadoClaseRepo;

    @Override
    public EstadoClase registrarEstado(EstadoClase estadoClase) throws AlreadyExistException {
        Optional<EstadoClase> buscado = estadoClaseRepo.findById(estadoClase.getCodigo());
        if(buscado.isPresent()){
            throw new AlreadyExistException("Ya existe el estado");
        }
        return estadoClaseRepo.save(estadoClase);
    }

    @Override
    public List<EstadoClase> listarEstados() {
        return estadoClaseRepo.findAll();
    }
}
