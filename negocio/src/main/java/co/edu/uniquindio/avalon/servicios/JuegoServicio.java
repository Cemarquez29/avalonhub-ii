package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Juego;

import java.util.List;

public interface JuegoServicio {
    Juego registarJuego(Juego juego);
    List<Juego> listarJuegos();
    Juego obtenerJuego(int juego);

}
