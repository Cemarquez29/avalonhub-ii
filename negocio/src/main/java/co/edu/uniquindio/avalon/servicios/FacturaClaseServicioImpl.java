package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.FacturaClase;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.repositorios.FacturaClaseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FacturaClaseServicioImpl implements FacturaClaseServicio {

    @Autowired
    private FacturaClaseRepo facturaClaseRepo;

    @Override
    public FacturaClase registrarFacturaClase(FacturaClase facturaClase) throws AlreadyExistException {
        Optional<FacturaClase> buscado = facturaClaseRepo.findById(facturaClase.getCodigo());
        if(buscado.isPresent()){
            throw new AlreadyExistException("Ya existe la factura");
        }
        return facturaClaseRepo.save(facturaClase);
    }

    @Override
    public List<FacturaClase> listarFacturas() {
        return facturaClaseRepo.findAll();
    }
}
