package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Juego;
import co.edu.uniquindio.avalon.repositorios.JuegoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JuegoServicioImpl implements JuegoServicio{

    @Autowired
    private JuegoRepo juegoRepo;

    @Override
    public Juego registarJuego(Juego juego) {
        return juegoRepo.save(juego);
    }

    @Override
    public List<Juego> listarJuegos() {
        return juegoRepo.findAll();
    }

    @Override
    public Juego obtenerJuego(int juego) {
        Optional<Juego> juegoOptional = juegoRepo.findById(juego);

        if(juegoOptional.isPresent())
            return juegoOptional.get();

        return null;
    }
}
