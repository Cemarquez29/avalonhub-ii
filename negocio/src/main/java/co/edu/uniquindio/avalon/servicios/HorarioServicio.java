package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Horario;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;

import java.util.List;

/**
 * Interface de HorarioServicio para Horario
 * @author Brian Giraldo - Cesar Marquez - Esteban Sanchez
 */
public interface HorarioServicio {

    Horario registrarHorario(Horario horario) throws AlreadyExistException;
    Horario actualizarHorario(Horario horario) throws NotExistException;
    void eliminarHorario(Horario horario) throws  NotExistException;
    List<Horario> listarHorarios();
    List<Horario> listarHorariosCoach(String coach);
}