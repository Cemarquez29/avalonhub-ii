package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;

public interface RecargaServicio {

    Recarga registrarRecarga(Recarga recarga, Usuario usuario);
    int obtenerCodigoSiquiente();
}
