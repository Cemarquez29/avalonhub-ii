package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Horario;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;
import co.edu.uniquindio.avalon.repositorios.HorarioRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HorarioServicioImpl implements HorarioServicio{

    @Autowired
    private HorarioRepo horarioRepo;

    @Override
    public Horario registrarHorario(Horario horario) throws AlreadyExistException {
        Optional<Horario> buscado = horarioRepo.findById(horario.getCodigo());
        if(buscado.isPresent()){
            throw new AlreadyExistException("Ya existe el horario");
        }
        return horarioRepo.save(horario);
    }

    @Override
    public Horario actualizarHorario(Horario horario) throws NotExistException {
        Optional<Horario> buscado = horarioRepo.findById(horario.getCodigo());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe el horario a actualizar");
        }
        return horarioRepo.save(horario);
    }

    @Override
    public void eliminarHorario(Horario horario) throws NotExistException {
        Optional<Horario> buscado = horarioRepo.findById(horario.getCodigo());
        if(buscado.isEmpty()){
            throw new NotExistException("No existe el horario");
        }
        horarioRepo.delete(horario);
    }

    @Override
    public List<Horario> listarHorarios() {
        return horarioRepo.findAll();
    }

    @Override
    public List<Horario> listarHorariosCoach(String coach) {
        return horarioRepo.obtenerHorariosCoach(coach);
    }
}
