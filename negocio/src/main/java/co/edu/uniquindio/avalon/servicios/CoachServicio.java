package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.entidades.Especialidad;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;
import co.edu.uniquindio.avalon.exceptions.NotExistException;

import java.util.List;

public interface CoachServicio {

    Coach registrarProfesor(Coach coach) throws AlreadyExistException;
    void eliminarProfesor(Coach coach) throws NotExistException;
    Coach actualizarProfesor(Coach coach) throws NotExistException;
    List<Coach> listarProfesores();
    List<Coach> listarProfesoresPorJuego(int juego);
    Especialidad especialidadPorJuego(int juego, String profesor);
    Coach obtenerCoach( String coach);

}
