package co.edu.uniquindio.avalon.servicios;

import co.edu.uniquindio.avalon.entidades.EstadoClase;
import co.edu.uniquindio.avalon.exceptions.AlreadyExistException;

import java.util.List;

public interface EstadoClaseServicio {

    EstadoClase registrarEstado(EstadoClase estadoClase) throws AlreadyExistException;
    List<EstadoClase> listarEstados();
}
