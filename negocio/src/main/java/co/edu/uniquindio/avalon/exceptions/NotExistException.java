package co.edu.uniquindio.avalon.exceptions;

public class NotExistException extends Exception{

    public NotExistException(String message) {
        super(message);
    }
}
