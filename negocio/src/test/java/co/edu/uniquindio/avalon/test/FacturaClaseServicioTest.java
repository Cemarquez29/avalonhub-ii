package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.*;
import co.edu.uniquindio.avalon.repositorios.*;
import co.edu.uniquindio.avalon.servicios.EstadoClaseServicio;
import co.edu.uniquindio.avalon.servicios.FacturaClaseServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class FacturaClaseServicioTest {

    @Autowired
    private FacturaClaseServicio facturaClaseServicio;
    @Autowired
    private FacturaClaseRepo facturaClaseRepo;
    @Autowired
    private CoachRepo coachRepo;
    @Autowired
    private ClaseRepo claseRepo;
    @Autowired
    private UsuarioRepo usuarioRepo;
    @Autowired
    private HorarioRepo horarioRepo;
    @Autowired
    private JuegoRepo juegoRepo;

    @Test
    void registrarFacturaClase(){

        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());
        horario.setCoach(coach);
        horarioRepo.save(horario);

        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        usuarioRepo.save(usuario);

        Juego juego = new Juego();
        juego.setNombre("Rust");
        juego.setDescripcion("Mundo abierto con disparos");
        juegoRepo.save(juego);

        Clase clase = new Clase();
        clase.setFecha(LocalDate.now());
        clase.setPagado(true);
        clase.setUsuario(usuario);
        clase.setCoach(coach);
        clase.setHorario(horario);
        clase.setJuego(juego);
        claseRepo.save(clase);

        FacturaClase facturaClase = new FacturaClase();
        facturaClase.setFecha(LocalDate.now());
        facturaClase.setValor(20000);
        facturaClase.setUsuario(usuario);
        facturaClase.setClase(clase);

        FacturaClase facturaClaseGuardada = null;

        try {
            facturaClaseGuardada = facturaClaseServicio.registrarFacturaClase(facturaClase);
            Assertions.assertNotNull(facturaClaseGuardada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void  listarFacturaClasesTest(){

        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());
        horario.setCoach(coach);
        horarioRepo.save(horario);

        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        usuarioRepo.save(usuario);

        Juego juego = new Juego();
        juego.setNombre("Rust");
        juego.setDescripcion("Mundo abierto con disparos");
        juegoRepo.save(juego);

        Clase clase = new Clase();
        clase.setFecha(LocalDate.now());
        clase.setPagado(true);
        clase.setUsuario(usuario);
        clase.setCoach(coach);
        clase.setHorario(horario);
        clase.setJuego(juego);
        claseRepo.save(clase);

        FacturaClase facturaClase = new FacturaClase();
        facturaClase.setFecha(LocalDate.now());
        facturaClase.setValor(20000);
        facturaClase.setUsuario(usuario);
        facturaClase.setClase(clase);

        FacturaClase facturaClaseGuardada = null;

        try {
            facturaClaseGuardada = facturaClaseServicio.registrarFacturaClase(facturaClase);
            List<FacturaClase> lista = facturaClaseServicio.listarFacturas();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
