package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.entidades.EstadoClase;
import co.edu.uniquindio.avalon.repositorios.CoachRepo;
import co.edu.uniquindio.avalon.repositorios.EstadoClaseRepo;
import co.edu.uniquindio.avalon.servicios.CoachServicio;
import co.edu.uniquindio.avalon.servicios.EstadoClaseServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class EstadoClaseServicioTest {

    @Autowired
    private EstadoClaseServicio estadoClaseServicio;
    @Autowired
    private EstadoClaseRepo estadoClaseRepo;

    @Test
    void registrarEstadoClase(){
        EstadoClase estadoClase = new EstadoClase();
        estadoClase.setNombre("Pendiente");
        estadoClase.setDescripcion("La clase esta pendiente a ser confirmada");

        EstadoClase estadoClaseGuardada = null;

        try {
            estadoClaseGuardada = estadoClaseServicio.registrarEstado(estadoClase);
            Assertions.assertNotNull(estadoClaseGuardada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void  listarEstadoClasesTest(){

        EstadoClase estadoClase = new EstadoClase();
        estadoClase.setNombre("Pendiente");
        estadoClase.setDescripcion("La clase esta pendiente a ser confirmada");

        EstadoClase estadoClaseGuardada = null;

        try {
            estadoClaseGuardada = estadoClaseServicio.registrarEstado(estadoClase);
            List<EstadoClase> lista = estadoClaseServicio.listarEstados();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
