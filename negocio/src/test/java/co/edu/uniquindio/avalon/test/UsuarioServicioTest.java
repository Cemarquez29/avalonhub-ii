package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.Clase;
import co.edu.uniquindio.avalon.entidades.Horario;
import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;
import co.edu.uniquindio.avalon.repositorios.UsuarioRepo;
import co.edu.uniquindio.avalon.servicios.RecargaServicio;
import co.edu.uniquindio.avalon.servicios.UsuarioServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class UsuarioServicioTest {

    @Autowired
    private UsuarioServicio usuarioServicio;

    @Autowired
    private RecargaServicio recargaServicio;

    @Autowired
    private UsuarioRepo usuarioRepo;

    @Test
    void registrarUsuarioTest(){
        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);

        Usuario usuarioGuardado = null;
        try {
            usuarioGuardado = usuarioServicio.registrarUsuario(usuario);
            Assertions.assertNotNull(usuarioGuardado);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void eliminarUsuarioTest() {
        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        Usuario usuarioGuardado = null;
        try {
            usuarioGuardado = usuarioServicio.registrarUsuario(usuario);
            usuarioServicio.eliminarUsuario(usuario);
            Usuario buscado = usuarioRepo.findById("Cemarquez").orElse(null);
            Assertions.assertNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void actualizarUsuarioTest() {
        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        Usuario usuarioGuardado = null;
        try {
            usuarioGuardado = usuarioServicio.registrarUsuario(usuario);
            usuario.setCorreo("cesar.marquez@gmail.com");
            usuarioServicio.actualizarUsuario(usuario);
            Usuario buscado = usuarioRepo.findByCorreo("cesar.marquez@gmail.com");
            Assertions.assertNotNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void obtenerUsuarioTest() {
        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        Usuario usuarioGuardado = null;
        try {
            usuarioGuardado = usuarioServicio.registrarUsuario(usuario);
            Usuario buscado = usuarioServicio.obtenerUsuario("Cemarquez");
            Assertions.assertNotNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void iniciarSesionTest() {
        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        Usuario usuarioGuardado = null;
        try {
            usuarioGuardado = usuarioServicio.registrarUsuario(usuario);
            Usuario buscado = usuarioServicio.iniciarSesion("Cemarquez","cesar");
            Assertions.assertNotNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void recuperarContraseniaTest() {
        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        Usuario usuarioGuardado = null;
        try {
            usuarioGuardado = usuarioServicio.registrarUsuario(usuario);
            Usuario buscado = usuarioServicio.recuperarContrasenia("Cemarquez");
            Assertions.assertNotNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void listarUsuariosTest() {

        Usuario usuario2 = new Usuario();
        usuario2.setUsername("Briando");
        usuario2.setCorreo("brian@gmail.com");
        usuario2.setContrasena("brian");
        usuario2.setTelefono("3106947999");
        usuario2.setSaldo(10000);
        Usuario usuarioGuardado2 = null;
        try {
            usuarioGuardado2 = usuarioServicio.registrarUsuario(usuario2);
            List<Usuario> lista = usuarioServicio.listarUsuarios();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void listarRecargasTest() {
        Usuario usuario = new Usuario();
        usuario.setUsername("Briando");
        usuario.setCorreo("brian@gmail.com");
        usuario.setContrasena("brian");
        usuario.setTelefono("3106947999");
        usuario.setSaldo(10000);
        Usuario usuarioGuardado = null;



        try {
            usuarioGuardado = usuarioServicio.registrarUsuario(usuario);

            Recarga recarga = new Recarga();
            recarga.setCodigo(2);
            recarga.setUsuario(usuario);
            recarga.setEstado(true);
            recarga.setValor(3000);
            recarga.setFecha(LocalDate.now());
            Recarga recargaGuardada = null;

            recargaGuardada = recargaServicio.registrarRecarga(recarga,usuario);

            List<Recarga> lista = usuarioServicio.listarRecargas("Briando");
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void listarClasesTest() {
        try{
            List<Clase> lista = usuarioServicio.listarClases("Cemarquez");
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void listarClasesCanceladasTest() {
        try {
            List<Clase> lista = usuarioServicio.listarClasesCanceladas("Cemarquez");
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
        e.printStackTrace();
        }
    }

    @Test
    void listarHorariosTest() {
        try{
            List<Horario> lista = usuarioServicio.listarHorario("RyanAim");
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
