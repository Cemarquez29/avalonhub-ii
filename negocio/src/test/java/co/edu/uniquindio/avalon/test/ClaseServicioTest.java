package co.edu.uniquindio.avalon.test;

import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.*;
import co.edu.uniquindio.avalon.repositorios.*;
import co.edu.uniquindio.avalon.servicios.ClaseServicio;
import co.edu.uniquindio.avalon.servicios.CoachServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class ClaseServicioTest {

    @Autowired
    private ClaseServicio claseServicio;

    @Autowired
    private ClaseRepo claseRepo;

    @Autowired
    private CoachRepo coachRepo;

    @Autowired
    private UsuarioRepo usuarioRepo;

    @Autowired
    private HorarioRepo horarioRepo;

    @Autowired
    private JuegoRepo juegoRepo;

    @Test
    void registrarClase(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());
        horario.setCoach(coach);
        horarioRepo.save(horario);

        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        usuarioRepo.save(usuario);

        Juego juego = new Juego();
        juego.setNombre("Rust");
        juego.setDescripcion("Mundo abierto con disparos");
        juegoRepo.save(juego);

        Clase clase = new Clase();
        clase.setFecha(LocalDate.now());
        clase.setPagado(true);
        clase.setUsuario(usuario);
        clase.setCoach(coach);
        clase.setHorario(horario);
        clase.setJuego(juego);

        Clase claseGuardada = null;

        try {
           claseGuardada = claseServicio.registrarClase(clase);
            Assertions.assertNotNull(claseGuardada);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void eliminarClase(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());
        horario.setCoach(coach);
        horarioRepo.save(horario);

        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        usuarioRepo.save(usuario);

        Juego juego = new Juego();
        juego.setNombre("Rust");
        juego.setDescripcion("Mundo abierto con disparos");
        juegoRepo.save(juego);

        Clase clase = new Clase();
        clase.setFecha(LocalDate.now());
        clase.setPagado(true);
        clase.setUsuario(usuario);
        clase.setCoach(coach);
        clase.setHorario(horario);
        clase.setJuego(juego);

        Clase claseGuardada = null;

        try {
            claseGuardada = claseServicio.registrarClase(clase);
            claseServicio.eliminarClase(claseGuardada);
            Clase buscada = claseRepo.findById(claseGuardada.getCodigo()).get();

            Assertions.assertNull(buscada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void actualizarClase(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());
        horario.setCoach(coach);
        horarioRepo.save(horario);

        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        usuarioRepo.save(usuario);

        Juego juego = new Juego();
        juego.setNombre("Rust");
        juego.setDescripcion("Mundo abierto con disparos");
        juegoRepo.save(juego);

        Clase clase = new Clase();
        clase.setFecha(LocalDate.now());
        clase.setPagado(true);
        clase.setUsuario(usuario);
        clase.setCoach(coach);
        clase.setHorario(horario);
        clase.setJuego(juego);
        Clase claseGuardada = null;

        try {
            claseGuardada = claseServicio.registrarClase(clase);

            claseGuardada.setDescripcion("Para jugar");
            claseServicio.actualizarClase(claseGuardada);
            Clase buscada = claseRepo.findById(claseGuardada.getCodigo()).get();

            Assertions.assertNotNull(buscada);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void listarClases(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());
        horario.setCoach(coach);
        horarioRepo.save(horario);

        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        usuarioRepo.save(usuario);

        Juego juego = new Juego();
        juego.setNombre("Rust");
        juego.setDescripcion("Mundo abierto con disparos");
        juegoRepo.save(juego);

        Clase clase = new Clase();
        clase.setFecha(LocalDate.now());
        clase.setPagado(true);
        clase.setUsuario(usuario);
        clase.setCoach(coach);
        clase.setHorario(horario);
        clase.setJuego(juego);
        Clase claseGuardada = null;

        try {
            claseGuardada = claseServicio.registrarClase(clase);

            List<Clase> clases = claseServicio.listarClase();
            System.out.println(clases);
            Assertions.assertNotNull(clases);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
