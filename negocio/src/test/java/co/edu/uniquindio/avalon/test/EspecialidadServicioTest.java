package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.entidades.Especialidad;
import co.edu.uniquindio.avalon.entidades.Juego;
import co.edu.uniquindio.avalon.repositorios.EspecialidadRepo;
import co.edu.uniquindio.avalon.servicios.CoachServicio;
import co.edu.uniquindio.avalon.servicios.EspecialidadServicio;
import co.edu.uniquindio.avalon.servicios.JuegoServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class EspecialidadServicioTest {

    @Autowired
    private EspecialidadServicio especialidadServicio;
    @Autowired
    private EspecialidadRepo especialidadRepo;
    @Autowired
    private JuegoServicio juegoServicio;
    @Autowired
    private CoachServicio coachServicio;

    @Test
    void registrarEspecialidadTest(){
        Juego juego = new Juego();
        juego.setDescripcion("El juego de acción por equipos del que fue pionero cuando salió hace más de 20 años");
        juego.setNombre("CSGO");

        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        try {
            coachServicio.registrarProfesor(coach);
            juegoServicio.registarJuego(juego);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Especialidad especialidad = new Especialidad();
        especialidad.setNombre("PVP");
        especialidad.setDescripcion("Dominio en los encuentros PVP con la todas las armas.");
        especialidad.setHorasJuego(12000);
        especialidad.setJuego(juego);
        especialidad.setCoach(coach);

        Especialidad especialidadGuardada = null;
        try {
            especialidadGuardada = especialidadServicio.registrarEspecialidad(especialidad);
            Assertions.assertNotNull(especialidadGuardada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void eliminarEspecialidadTest(){
        Juego juego = new Juego();
        juego.setDescripcion("El juego de acción por equipos del que fue pionero cuando salió hace más de 20 años");
        juego.setNombre("CSGO");

        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        try {
            coachServicio.registrarProfesor(coach);
            juegoServicio.registarJuego(juego);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Especialidad especialidad = new Especialidad();
        especialidad.setNombre("PVP");
        especialidad.setDescripcion("Dominio en los encuentros PVP con la todas las armas.");
        especialidad.setHorasJuego(12000);
        especialidad.setJuego(juego);
        especialidad.setCoach(coach);

        Especialidad especialidadGuardada = null;
        try {
            especialidadGuardada = especialidadServicio.registrarEspecialidad(especialidad);
            especialidadServicio.eliminarEspecialidad(especialidad);
            Especialidad buscada = especialidadRepo.findById(especialidadGuardada.getCodigo()).orElse(null);
            Assertions.assertNull(buscada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void actualizarEspecialidadTest(){
        Juego juego = new Juego();
        juego.setDescripcion("El juego de acción por equipos del que fue pionero cuando salió hace más de 20 años");
        juego.setNombre("CSGO");

        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        try {
            coachServicio.registrarProfesor(coach);
            juegoServicio.registarJuego(juego);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Especialidad especialidad = new Especialidad();
        especialidad.setNombre("PVP");
        especialidad.setDescripcion("Dominio en los encuentros PVP con la todas las armas.");
        especialidad.setHorasJuego(12000);
        especialidad.setJuego(juego);
        especialidad.setCoach(coach);

        Especialidad especialidadGuardada = null;
        Especialidad especialidadActualizada = null;
        try {
            especialidadGuardada = especialidadServicio.registrarEspecialidad(especialidad);
            especialidad.setNombre("PVP MASTER");
            especialidadActualizada = especialidadServicio.actualizarEspecialidad(especialidad);
            Assertions.assertEquals(especialidadActualizada.getNombre(),"PVP MASTER");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void listarEspecialidadesTest(){
        try {
            List<Especialidad> lista = especialidadServicio.listarEspecialidades();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
