package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.repositorios.CoachRepo;
import co.edu.uniquindio.avalon.servicios.CoachServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class CoachServicioTest {

    @Autowired
    private CoachServicio coachServicio;
    @Autowired
    private CoachRepo coachRepo;

    @Test
    void registrarProfesorTest(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");

        Coach coachGuardado = null;
        try {
            coachGuardado = coachServicio.registrarProfesor(coach);
            Assertions.assertNotNull(coachGuardado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void eliminarProfesorTest(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");

        Coach coachGuardado = null;
        try {
            coachGuardado = coachServicio.registrarProfesor(coach);
            coachServicio.eliminarProfesor(coach);
            Coach buscado = coachRepo.findById("OSAKARAIN").orElse(null);
            Assertions.assertNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void actualizarProfesorTest(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");

        Coach coachGuardado = null;
        try {
            coachGuardado = coachServicio.registrarProfesor(coach);
            coach.setCorreo("estabann@gmail.com");
            coachServicio.actualizarProfesor(coach);
            Coach buscado = coachRepo.findByCorreo("estabann@gmail.com");
            Assertions.assertNotNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void obtenerCoachTest(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");

        Coach coachGuardado = null;
        try {
            coachGuardado = coachServicio.registrarProfesor(coach);
            Coach buscado = coachServicio.obtenerCoach("OSAKARAIN");
            Assertions.assertNotNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void  listarProfesoresTest(){
        try {
            List<Coach> lista = coachServicio.listarProfesores();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void  listarProfesoresPorJuegoTest(){
        try {
            List<Coach> lista = coachServicio.listarProfesoresPorJuego(4);
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
