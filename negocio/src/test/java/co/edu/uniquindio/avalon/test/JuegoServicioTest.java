package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.Categoria;
import co.edu.uniquindio.avalon.entidades.Juego;
import co.edu.uniquindio.avalon.repositorios.CategoriaRepo;
import co.edu.uniquindio.avalon.repositorios.JuegoRepo;
import co.edu.uniquindio.avalon.servicios.CategoriaServicio;
import co.edu.uniquindio.avalon.servicios.JuegoServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class JuegoServicioTest {

    @Autowired
    private JuegoServicio juegoServicio;

    @Autowired
    private JuegoRepo juegoRepo;

    @Autowired
    private CategoriaRepo categoriaRepo;

    @Test
    void registrarJuegoTest(){
        Categoria categoria = new Categoria();
        categoria.setDescripcion("Juegos de disparos en primera y tercera persona");
        categoria.setNombre("Disparos");
        categoriaRepo.save(categoria);

        Juego juego = new Juego();
        juego.setNombre("Resident Evil 4");
        juego.setDescripcion("Shooter zombie en tercera persona");
        juego.setCategoria(categoria);

        Juego juegoGuardado = null;

        try {
            juegoGuardado = juegoServicio.registarJuego(juego);
            Assertions.assertNotNull(juegoGuardado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void eliminarJuegoTest() {
        Categoria categoria = new Categoria();
        categoria.setDescripcion("Juegos de disparos en primera y tercera persona");
        categoria.setNombre("Disparos");
        categoriaRepo.save(categoria);

        Juego juego = new Juego();
        juego.setNombre("Resident Evil 4");
        juego.setDescripcion("Shooter zombie en tercera persona");
        juego.setCategoria(categoria);

        Juego juegoGuardado = null;

        try {
            juegoGuardado = juegoServicio.registarJuego(juego);
            juegoRepo.delete(juego);
            Juego buscado = juegoRepo.findById(juegoGuardado.getCodigo()).orElse(null);
            Assertions.assertNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void obtenerJuegoTest() {
        Categoria categoria = new Categoria();
        categoria.setDescripcion("Juegos de disparos en primera y tercera persona");
        categoria.setNombre("Disparos");
        categoriaRepo.save(categoria);

        Juego juego = new Juego();
        juego.setNombre("Resident Evil 4");
        juego.setDescripcion("Shooter zombie en tercera persona");
        juego.setCategoria(categoria);

        Juego juegoGuardado = null;

        try {
            juegoGuardado = juegoServicio.registarJuego(juego);
            Juego buscado = juegoServicio.obtenerJuego(juegoGuardado.getCodigo());
            Assertions.assertNotNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void listarJuegoTest() {
        try {
            List<Juego> lista = juegoServicio.listarJuegos();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
