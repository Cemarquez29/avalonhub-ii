package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;
import co.edu.uniquindio.avalon.repositorios.CoachRepo;
import co.edu.uniquindio.avalon.repositorios.RecargaRepo;
import co.edu.uniquindio.avalon.repositorios.UsuarioRepo;
import co.edu.uniquindio.avalon.servicios.CoachServicio;
import co.edu.uniquindio.avalon.servicios.RecargaServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class RecargaServicioTest {

    @Autowired
    private RecargaServicio recargaServicio;
    @Autowired
    private RecargaRepo recargaRepo;
    @Autowired
    private UsuarioRepo usuarioRepo;

    @Test
    void registrarRecargasTest(){
        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        usuarioRepo.save(usuario);

        Recarga recarga = new Recarga();
        recarga.setFecha(LocalDate.now());
        recarga.setValor(2200);
        recarga.setEstado(true);

        Recarga recargaGuardada = null;
        try {
            recargaGuardada = recargaServicio.registrarRecarga(recarga, usuario);
            Assertions.assertNotNull(recargaGuardada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void obtenerRecargaTest(){
        Usuario usuario = new Usuario();
        usuario.setUsername("Cemarquez");
        usuario.setCorreo("cesar@gmail.com");
        usuario.setContrasena("cesar");
        usuario.setTelefono("3213398080");
        usuario.setSaldo(7800);
        usuarioRepo.save(usuario);

        Recarga recarga = new Recarga();
        recarga.setFecha(LocalDate.now());
        recarga.setValor(2200);
        recarga.setEstado(true);

        Recarga recargaGuardada = null;
        try {
            recargaGuardada = recargaServicio.registrarRecarga(recarga, usuario);
            Recarga buscado = recargaRepo.findById(recargaGuardada.getCodigo()).orElse(null);
            Assertions.assertNotNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void  listarRecargasTest(){
        try {
            List<Recarga> lista = recargaRepo.findAll();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
