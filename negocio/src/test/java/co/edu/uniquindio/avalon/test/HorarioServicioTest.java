package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.entidades.Horario;
import co.edu.uniquindio.avalon.repositorios.CoachRepo;
import co.edu.uniquindio.avalon.repositorios.HorarioRepo;
import co.edu.uniquindio.avalon.servicios.CoachServicio;
import co.edu.uniquindio.avalon.servicios.HorarioServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class HorarioServicioTest {

    @Autowired
    private HorarioServicio horarioServicio;
    @Autowired
    private HorarioRepo horarioRepo;
    @Autowired
    private CoachRepo coachRepo;

    @Test
    public void registrarHorarioTest(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setCoach(coach);
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());

        Horario horarioGuradado = null;

        try {
            horarioGuradado = horarioServicio.registrarHorario(horario);
            Assertions.assertNotNull(horarioGuradado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void eliminarHorarioTest(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setCoach(coach);
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());

        Horario horarioGuradado = null;

        try {
            horarioGuradado = horarioServicio.registrarHorario(horario);
            horarioServicio.eliminarHorario(horario);
            Horario buscado = horarioRepo.findById(horarioGuradado.getCodigo()).orElse(null);
            Assertions.assertNull(buscado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void actualizarHorarioTest(){
        Coach coach1 = new Coach();
        coach1.setCorreo("gerardo.lopez@gmail.com");
        coach1.setNombre("Gerardo Lopez");
        coach1.setTelefono("3113352647");
        coach1.setUsername("GERARLOP");
        coach1.setCedula("298456512");
        coachRepo.save(coach1);

        Coach coach2 = new Coach();
        coach2.setCorreo("esteban.carrancha@gmail.com");
        coach2.setNombre("Esteban Carrancha");
        coach2.setTelefono("3002581120");
        coach2.setUsername("OSAKARAIN");
        coach2.setCedula("1001345102");
        coachRepo.save(coach2);

        Horario horario = new Horario();
        horario.setCoach(coach1);
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());

        Horario horarioGuradado = null;
        Horario horarioActualizado = null;

        try {
            horarioGuradado = horarioServicio.registrarHorario(horario);
            horario.setCoach(coach2);
            horarioActualizado = horarioServicio.actualizarHorario(horario);
            Assertions.assertEquals(horarioActualizado.getCoach().getUsername(),"OSAKARAIN");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void  listarHorarioTest(){
        try {
            List<Horario> lista = horarioServicio.listarHorarios();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void  listarHorarioCoachTest(){
        Coach coach = new Coach();
        coach.setCorreo("esteban.carrancha@gmail.com");
        coach.setNombre("Esteban Carrancha");
        coach.setTelefono("3002581120");
        coach.setUsername("OSAKARAIN");
        coach.setCedula("1001345102");
        coachRepo.save(coach);

        Horario horario = new Horario();
        horario.setCoach(coach);
        horario.setHoraInicio(LocalTime.now());
        horario.setDia(LocalDate.now());

        Horario horarioGuradado = null;

        try {
            horarioGuradado = horarioServicio.registrarHorario(horario);
            List<Horario> lista = horarioServicio.listarHorariosCoach(coach.getUsername());
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
