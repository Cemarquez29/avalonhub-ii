package co.edu.uniquindio.avalon.test;


import co.edu.uniquindio.avalon.NegocioApplication;
import co.edu.uniquindio.avalon.entidades.Categoria;
import co.edu.uniquindio.avalon.repositorios.CategoriaRepo;
import co.edu.uniquindio.avalon.servicios.CategoriaServicio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest(classes = NegocioApplication.class)
@Transactional
class CategoriaServicioTest {

    @Autowired
    private CategoriaServicio categoriaServicio;

    @Autowired
    private CategoriaRepo categoriaRepo;

    @Test
    void registrarCategoriaTest(){
        Categoria categoria = new Categoria();
        categoria.setDescripcion("Juegos de disparos en primera y tercera persona");
        categoria.setNombre("Disparos");

        Categoria categoriaGuardado = null;
        try {
            categoriaGuardado = categoriaServicio.registrarCategoria(categoria);
            Assertions.assertNotNull(categoriaGuardado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void eliminarCategoriaTest() {
        Categoria categoria = new Categoria();
        categoria.setDescripcion("Juegos de disparos en primera y tercera persona");
        categoria.setNombre("Disparos");

        Categoria categoriaGuardado = null;
        try {
            categoriaGuardado = categoriaServicio.registrarCategoria(categoria);
            categoriaRepo.delete(categoria);
            Categoria buscada = categoriaRepo.findById(categoriaGuardado.getCodigo()).orElse(null);
            Assertions.assertNull(buscada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void obtenerCategoriaTest(){
        Categoria categoria = new Categoria();
        categoria.setDescripcion("Juegos de disparos en primera y tercera persona");
        categoria.setNombre("Disparos");
        Categoria categoriaGuardado = null;
        try {
            categoriaGuardado = categoriaServicio.registrarCategoria(categoria);
            Categoria buscada = categoriaServicio.obtenerCategoria(categoriaGuardado.getCodigo());
            Assertions.assertNotNull(buscada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void listarCategoriasTest() {
        Categoria categoria1 = new Categoria();
        categoria1.setDescripcion("Juegos de disparos en primera y tercera persona");
        categoria1.setNombre("Disparos");
        Categoria categoriaGuardado1 = null;

        Categoria categoria2 = new Categoria();
        categoria2.setDescripcion("Juegos de misterios y puzzles");
        categoria2.setNombre("Misterio");
        Categoria categoriaGuardado2 = null;
        try {
            categoriaGuardado1 = categoriaServicio.registrarCategoria(categoria1);
            categoriaGuardado2 = categoriaServicio.registrarCategoria(categoria2);
            List<Categoria> lista = categoriaServicio.listaCategorias();
            System.out.println(lista);
            Assertions.assertNotNull(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
