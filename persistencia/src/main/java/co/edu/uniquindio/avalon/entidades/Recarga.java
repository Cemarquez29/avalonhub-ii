package co.edu.uniquindio.avalon.entidades;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class Recarga implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codigo", nullable = false)
    @EqualsAndHashCode.Include
    private int codigo;


    @ManyToOne
    @JoinColumn(name= "username_usuario",nullable = false)
    private Usuario usuario;

    @Column(name = "fecha", nullable = false)
    private LocalDate fecha;

    @Column(name = "valor", nullable = false)
    private double valor;

    @Column(name = "estado",nullable = false)
    private boolean estado;

    @OneToOne
    @JoinColumn(name="codigo_factura")
    private FacturaRecarga facturaRecarga;




}
