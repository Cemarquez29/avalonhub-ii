package co.edu.uniquindio.avalon.entidades;

import java.io.Serializable;
import java.time.LocalDate;
import lombok.*;
import javax.persistence.*;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class FacturaClase implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codigo", nullable = false)
    @EqualsAndHashCode.Include
    private int codigo;

    @Column(name = "fecha", nullable = false)
    private LocalDate fecha;

    @Column(name="valor",nullable = false)
    private double valor;


    @OneToOne
    @JoinColumn(name="codigo_clase",nullable = false)
    private Clase clase;

    @ManyToOne
    @JoinColumn(name="username_usuario", nullable = false)
    private Usuario usuario;



}
