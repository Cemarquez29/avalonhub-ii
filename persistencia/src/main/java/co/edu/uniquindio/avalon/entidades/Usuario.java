package co.edu.uniquindio.avalon.entidades;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.List;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class Usuario implements Serializable {

    @Id
    @Column(name="username", nullable = false)
    @EqualsAndHashCode.Include
    private String username;



    @Column(name="correo",nullable = false)
    @Email
    private String correo;

    @Column(name="contrasena", nullable = false)
    private String contrasena;

    @Column(name="telefono")
    private String telefono;

    @Column(name="saldo",nullable = false)
    private double saldo;



    @OneToMany(mappedBy = "usuario")
    @ToString.Exclude
    private List<FacturaRecarga> facturasRecargas;

    @OneToMany(mappedBy = "usuario")
    @ToString.Exclude
    private List<FacturaClase> facturasClases;

    @OneToMany(mappedBy = "usuario")
    @ToString.Exclude
    private List<Recarga> listaRecargas;

    @OneToMany(mappedBy = "usuario")
    @ToString.Exclude
    private List<Clase> listaClases;


    public Usuario(String username, String correo, String contrasena, String telefono, double saldo) {
        this.username = username;
        this.correo = correo;
        this.contrasena = contrasena;
        this.telefono = telefono;
        this.saldo = saldo;
    }
}
