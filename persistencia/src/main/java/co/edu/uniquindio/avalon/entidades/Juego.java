package co.edu.uniquindio.avalon.entidades;

import lombok.*;
import javax.persistence.*;
import java.awt.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class Juego  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codigo", nullable = false)
    @EqualsAndHashCode.Include
    private int codigo;


    @Column(name="nombre",nullable = false)
    private String nombre;

    @Column(name="descripcion",nullable = false)
    private String descripcion;

    @OneToOne
    @JoinColumn(name="codigo_categoria")
    private Categoria categoria;

    @OneToMany(mappedBy = "juego")
    @ToString.Exclude
    private List<Clase> listaClases;

}
