package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.Juego;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JuegoRepo extends JpaRepository<Juego,Integer> {



}
