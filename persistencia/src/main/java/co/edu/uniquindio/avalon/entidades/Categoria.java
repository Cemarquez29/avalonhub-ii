package co.edu.uniquindio.avalon.entidades;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class Categoria implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codigo", nullable = false)
    @EqualsAndHashCode.Include
    private int codigo;

    @Column(name="nombre",nullable = false)
    private String nombre;

    @Column(name="descripcion",nullable = false)
    private String descripcion;




}
