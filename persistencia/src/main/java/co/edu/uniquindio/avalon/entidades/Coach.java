package co.edu.uniquindio.avalon.entidades;

import java.io.Serializable;
import java.util.List;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.Email;
@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class Coach implements Serializable {

    @Id
    @Column(name="username", nullable = false)
    @EqualsAndHashCode.Include
    private String username;

    @Column(name="cedula",nullable = false)
    private String cedula;

    @Column(name="correo",nullable = false)
    @Email
    private String correo;


    @Column(name="telefono")
    private String telefono;

    @Column(name="nombre")
    private String nombre;

    @ToString.Exclude
    @OneToMany (mappedBy = "coach")
    private List<Horario> listaHorarios;

    @ToString.Exclude
    @OneToMany(mappedBy = "coach")
    private List<Especialidad> listaEspecialidades;

    @ToString.Exclude
    @OneToMany(mappedBy = "coach")
    private List<Clase> listaClases;





}
