package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.entidades.Recarga;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecargaRepo extends JpaRepository<Recarga,Integer> {

    @Query("select r.codigo from Recarga r order by r.codigo desc")
    List<Integer> obtenerCodigoSiguiente();

}
