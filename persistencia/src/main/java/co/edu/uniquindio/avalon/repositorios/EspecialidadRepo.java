package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.Especialidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspecialidadRepo extends JpaRepository<Especialidad,Integer> {



}
