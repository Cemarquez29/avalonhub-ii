package co.edu.uniquindio.avalon.entidades;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import lombok.*;
import javax.persistence.*;
@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class Horario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codigo", nullable = false)
    @EqualsAndHashCode.Include
    private int codigo;

    @Column(name = "dia" , nullable = false)
    private LocalDate dia;

    @Column(name = "hora_inicio", nullable = false)
    private LocalTime horaInicio;

    @ManyToOne
    @JoinColumn(name="username_coach" ,nullable = false)
    private Coach coach;




}
