package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.Clase;
import co.edu.uniquindio.avalon.entidades.Horario;
import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepo extends JpaRepository<Usuario,String> {

    Usuario findByUsernameAndContrasena(String username,String contrasena);

    Usuario findByCorreo(String correo);
    Usuario findByUsername(String username);

    @Query ("select r from Recarga r WHERE r.usuario.username = ?1")
    List<Recarga> listarRecargas (String usuario);

    @Query ("select c from Clase c WHERE c.usuario.username = ?1 AND c.pagado = true")
    List<Clase> listarClases (String usuario);

    @Query ("select c from Clase c  WHERE c.usuario.username = ?1 AND c.estadoClase.nombre = 'CANCELADA'")
    List<Clase> listarClasesCanceladas (String usuario);

    @Query ("select h from Horario h JOIN Clase c ON h.codigo = c.horario.codigo WHERE c.usuario.username= ?1 AND c.pagado = true")
    List<Horario> listarHorarios (String usuario);
}
