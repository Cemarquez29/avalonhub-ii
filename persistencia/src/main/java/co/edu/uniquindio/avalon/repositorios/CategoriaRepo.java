package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriaRepo extends JpaRepository<Categoria,Integer> {

    Categoria findByNombre (String categoria);


}
