package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.FacturaRecarga;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacturaRecargaRepo extends JpaRepository<FacturaRecarga,Integer> {



}
