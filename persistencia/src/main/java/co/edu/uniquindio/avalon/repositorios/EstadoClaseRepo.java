package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.EstadoClase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoClaseRepo extends JpaRepository<EstadoClase,Integer> {



}
