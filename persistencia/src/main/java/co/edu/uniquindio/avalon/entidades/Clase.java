package co.edu.uniquindio.avalon.entidades;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.*;

import javax.persistence.*;
@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class Clase  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codigo", nullable = false)
    @EqualsAndHashCode.Include
    private int codigo;

    @Column(name = "fecha",nullable = false)
    private LocalDate fecha;

    @Column(name="descripcion")
    private String descripcion;

    @Column(name="pagado",nullable = false)
    private boolean pagado;


    @ManyToOne
    @JoinColumn(name="codigo_juego",nullable = false)
    private Juego juego;

    @ManyToOne
    @JoinColumn(name="username_usuario",nullable = false)
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name="username_coach",nullable = false)
    private Coach coach;


    @ManyToOne
    @JoinColumn(name="codigo_estado")
    private EstadoClase estadoClase;

    @OneToOne
    @JoinColumn(name = "codigo_horario",nullable = false)
    private Horario horario;


    public Clase(int codigo, boolean pagado, Usuario usuario) {
        this.codigo = codigo;
        this.pagado = pagado;
        this.usuario = usuario;
    }
}
