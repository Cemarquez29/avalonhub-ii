package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.FacturaClase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacturaClaseRepo extends JpaRepository<FacturaClase,Integer> {



}
