package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.Coach;
import co.edu.uniquindio.avalon.entidades.Especialidad;
import co.edu.uniquindio.avalon.entidades.Horario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CoachRepo extends JpaRepository<Coach,String> {

    Coach findByCorreo(String correo);

    @Query("select e.coach from Especialidad e WHERE e.juego.codigo = ?1")
    List<Coach> listarProfesoresPorJuego(int juego);

    @Query("select l FROM Especialidad l WHERE l.juego.codigo = ?1 AND  l.coach.username = ?2")
    Especialidad EspecialidadPorJuego(int juego, String profesor);
}
