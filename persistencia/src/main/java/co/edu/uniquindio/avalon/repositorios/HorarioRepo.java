package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.Horario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HorarioRepo extends JpaRepository<Horario,Integer> {

    @Query("SELECT h FROM Horario h WHERE h.coach.username = ?1")
    List<Horario> obtenerHorariosCoach(String coach);



}
