package co.edu.uniquindio.avalon.entidades;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class Especialidad implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codigo", nullable = false)
    @EqualsAndHashCode.Include
    private int codigo;

    @Column(name="nombre",nullable = false)
    private String nombre;

    @Column(name="descripcion",nullable = false)
    private String descripcion;

    @JoinColumn(name="codigo_juego", nullable = false)
    @OneToOne
    private Juego juego;

    @Column(name="horas_juego",nullable = false)
    private int horasJuego;


    @ManyToOne
    @JoinColumn(name="username_coach",nullable = false)
    private Coach coach;

}
