package co.edu.uniquindio.avalon.repositorios;

import co.edu.uniquindio.avalon.entidades.Clase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClaseRepo extends JpaRepository<Clase,Integer> {




}
