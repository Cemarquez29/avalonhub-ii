package co.edu.uniquindio.avalon.entidades;

import java.io.Serializable;
import java.util.List;

import lombok.*;
import javax.persistence.*;
@Entity
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
@ToString
public class EstadoClase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "codigo", nullable = false)
    @EqualsAndHashCode.Include
    private int codigo;


    @Column(name="nombre",nullable = false)
    private String nombre;

    @Column(name="descripcion",nullable = false)
    private String descripcion;


    @OneToMany(mappedBy = "estadoClase")
    @ToString.Exclude
    private List<Clase> listaClases;


}
