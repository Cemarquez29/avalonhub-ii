package co.edu.uniquindio.avalon.bean;

import co.edu.uniquindio.avalon.entidades.*;
import co.edu.uniquindio.avalon.servicios.*;
import io.micrometer.core.annotation.Timed;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
@ViewScoped
public class HorarioBean implements Serializable {

    @Value(value ="#{loginBean.usuario}")
    @Getter
    @Setter
    private Usuario usuario;

    private static final Logger LOGGER = Logger.getLogger(HorarioBean.class.getName());

    private final String perfilURL = "/usuario/perfil?faces-redirect=true";

    @Autowired
    private transient HorarioServicio horarioServicio;

    @Autowired
    private transient JuegoServicio juegoServicio;

    @Autowired
    private transient CoachServicio coachServicio;

    @Autowired
    private transient ClaseServicio claseServicio;

    @Value("#{param['coach']}")
    private String coachUsername;

    @Value("#{param['juego']}")
    private String idJuego;

    @Getter
    @Setter
    private LocalDate fechaActual;

    @Getter
    @Setter
    private LocalDate fechaClase;

    @Getter
    @Setter
    private String fechaClaseString;


    private List<Horario> horariosOcupados;

    @PostConstruct
    public void init(){
        this.horariosOcupados = horarioServicio.listarHorariosCoach(coachUsername);
        this.fechaClase = LocalDate.now();
        this.fechaActual = LocalDate.now();


    }


    public List<String> listaHorarios(){
        List<String> horarios = new ArrayList<>();
        List<LocalTime> horariosOcupadosDia = horariosOcupadosDia();

        for(int i=10; i<=17; i++){
            if(!isOcupado(i, horariosOcupadosDia)){
                horarios.add(i+":00");
            }
        }
        return horarios;
    }

    public boolean isOcupado(int hour, List<LocalTime> horarios){
        for (LocalTime h : horarios){
            if(h.getHour() == hour)
                return true;
        }
        return false;
    }

    public List<LocalTime> horariosOcupadosDia(){
        List<LocalTime> horariosOcupadosDia = new ArrayList<>();
        for(Horario h : horariosOcupados){
            if(fechaClase.isEqual(h.getDia())){
                horariosOcupadosDia.add(h.getHoraInicio());
            }
        }

        return horariosOcupadosDia;
    }

    public String irPerfil(){
        return perfilURL;
    }

    @Timed(value = "registrarclase.time", description = "Tiempo de respuesta de registrar clase")
    public String registrarClase(){
        if(usuario != null){
            Coach coach = coachServicio.obtenerCoach(coachUsername);
            Juego juego = juegoServicio.obtenerJuego(Integer.parseInt(idJuego));
            TimeZone.setDefault(TimeZone.getTimeZone("GMT-5"));
            LocalTime horaClase = LocalTime.parse(fechaClaseString);
            Horario horario = new Horario();
            horario.setDia(fechaClase);
            horario.setCoach(coach);
            horario.setHoraInicio(horaClase);
            try {
                horarioServicio.registrarHorario(horario);
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, e.getMessage());
            }

            Clase clase = new Clase();
            clase.setCoach(coach);
            clase.setJuego(juego);
            clase.setFecha(fechaClase);
            clase.setPagado(true);
            clase.setUsuario(usuario);
            clase.setHorario(horario);
            try {
                claseServicio.registrarClase(clase);
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, e.getMessage());
            }

            return "/usuario/perfil?faces-redirect=true";
        }

        return null;
    }

}
