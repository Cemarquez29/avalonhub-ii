package co.edu.uniquindio.avalon.bean;

import co.edu.uniquindio.avalon.entidades.Usuario;
import co.edu.uniquindio.avalon.servicios.UsuarioServicio;
import io.micrometer.core.annotation.Timed;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@Component
@Scope("session")
public class LoginBean implements Serializable {

    @Autowired
    private transient UsuarioServicio usuarioServicio;

    @Getter @Setter
    private Usuario usuario;

    @Getter @Setter
    private String email;

    @Getter @Setter
    private String password;

    @Getter @Setter
    private String asunto;

    @Getter @Setter
    private String mensaje;

    @Getter @Setter
    private String emailContacto;

    @Getter @Setter
    private boolean autenticado;

    private static final String msgAlerta = "Alerta";

    @PostConstruct
    public void inicializar(){
        this.usuario = new Usuario();
        this.autenticado = false;
    }
    @Timed(value = "iniciarsesion.time", description = "Tiempo de respuesta del inicio de sesión")
    public String iniciarSesion() {
        if (email != null && password!=null){
            try {
                usuario = usuarioServicio.iniciarSesion(email,password);
                if (usuario!=null){
                    autenticado=true;
                    return "/usuario/perfil?faces-redirect=true";
                } else{
                    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msgAlerta,
                            "¡Usuario o contraseña incorrectos!");
                    FacesContext.getCurrentInstance().addMessage("login-bean",facesMsg);
                    return null;
                }

            } catch (Exception e) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,msgAlerta,e.getMessage());
                FacesContext.getCurrentInstance().addMessage("login-bean",m);
            }
        }
        return null;
    }

    public String cerrarSesion() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/index?faces-redirect=true";
    }
    @Timed(value = "recuperarcontrasenia.time", description = "Tiempo de respuesta de recuperar contrasenia")
    public String recuperarContrasenia() {

        if (email != null){

            try {

                usuario = usuarioServicio.recuperarContrasenia(email);

                if (usuario!=null){
                    usuario.setContrasena(EmailBean.getPassword());
                    usuarioServicio.actualizarUsuario(usuario);
                    EmailBean.sendEmailContraseña(usuario.getCorreo(), usuario.getContrasena());
                    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msgAlerta,
                            "¡Contraseña enviada a tu email!");
                    FacesContext.getCurrentInstance().addMessage("olvidaste-bean", facesMsg);
                    return null;
                } else{
                    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msgAlerta,
                            "¡Dirección de correo o usuario incorrecto!");
                    FacesContext.getCurrentInstance().addMessage("olvidaste-bean",facesMsg);
                    return null;
                }


            } catch (Exception e) {

                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,msgAlerta,e.getMessage());
                FacesContext.getCurrentInstance().addMessage("olvidaste-bean",m);
            }

        }

        return null;
    }
    @Timed(value = "contactaravalon.time", description = "Tiempo de respuesta de contactar")
    public String contactarAvalon() {

        EmailBean.sendEmailContacto(emailContacto, mensaje, asunto);
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msgAlerta,
                "¡Se ha enviado el mensaje!");
        FacesContext.getCurrentInstance().addMessage("contactar-bean", facesMsg);

        if(autenticado){
            return "/usuario/perfil?faces-redirect=true";
        } else {
            return "/index?faces-redirect=true";
        }
    }

}
