package co.edu.uniquindio.avalon.bean;

import co.edu.uniquindio.avalon.entidades.*;
import co.edu.uniquindio.avalon.servicios.JuegoServicio;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.util.List;

@Component
@ViewScoped
public class ClasesBean implements Serializable {

    @Value(value ="#{loginBean.usuario}")
    @Getter
    @Setter
    private Usuario usuario;

    @Getter @Setter
    private List<Juego> listaJuegos;

    @Autowired
    private transient JuegoServicio juegoServicio;

    @PostConstruct
    public void init(){
        this.listaJuegos = juegoServicio.listarJuegos();
    }

    public String filtrarProfesores(int juego)
    {
       return "/usuario/elegirProfesor?faces-redirect=true&amp;juego=" + juego;
    }
}
