package co.edu.uniquindio.avalon.bean;

import co.edu.uniquindio.avalon.entidades.Clase;
import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;
import co.edu.uniquindio.avalon.exceptions.NotExistException;
import co.edu.uniquindio.avalon.servicios.UsuarioServicio;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.util.List;

@Component
@ViewScoped
public class PerfilBean implements Serializable {

    @Value(value ="#{loginBean.usuario}")
    @Getter @Setter
    private Usuario usuario;

    @Autowired
    private transient UsuarioServicio usuarioServicio;

    @Getter @Setter
    private List<Recarga> listaRecargas;

    @Getter @Setter
    private List<Clase> listaClases;

    @Getter @Setter
    private List<Clase> clasesCanceladas;

    @Getter @Setter
    private List<Clase> clasesNoPagas;

    private static final String recargaURL = "/usuario/recargarSaldo?faces-redirect=true";

    @PostConstruct
    public void init(){
        this.listaRecargas = usuarioServicio.listarRecargas(usuario.getUsername());
        this.listaClases = usuarioServicio.listarClases(usuario.getUsername());
    }

    public void actualizarUsuario() throws NotExistException {
            usuarioServicio.actualizarUsuario(usuario);
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alerta","Actualización exitosa");
            FacesContext.getCurrentInstance().addMessage(null, facesMsg);

    }

    public String irRecargar(){
        return recargaURL;
    }

}
