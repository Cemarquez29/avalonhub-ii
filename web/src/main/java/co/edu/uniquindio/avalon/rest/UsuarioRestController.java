package co.edu.uniquindio.avalon.rest;

import co.edu.uniquindio.avalon.entidades.Clase;
import co.edu.uniquindio.avalon.servicios.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/usuario")
public class UsuarioRestController {


    @Autowired
    private UsuarioServicio usuarioServicio;






    @GetMapping("clases/{nickname}")
    public List<Clase> listarClases(@PathVariable(name = "nickname") String nickname){

        return  usuarioServicio.listarClases(nickname);


    }




}
