package co.edu.uniquindio.avalon.filter;

import co.edu.uniquindio.avalon.bean.EmailBean;
import co.edu.uniquindio.avalon.bean.LoginBean;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class SeguridadFilter implements Filter {
    public static final String PAGINA_INICIO = "/index.xhtml";
    private static final Logger LOGGER = Logger.getLogger(SeguridadFilter.class.getName());
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        ( (HttpServletResponse) servletResponse).setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        ( (HttpServletResponse) servletResponse).setHeader("Pragma", "no-cache");
        try {
            final HttpServletRequest request = (HttpServletRequest) servletRequest;
            final HttpServletResponse response = (HttpServletResponse) servletResponse;
            final String requestURI = request.getRequestURI();
            //Aplicar el filtro a esta carpeta
            if (requestURI.startsWith("/usuario/") ) {
                //Obtenemos el objeto seguridadBean de la sesión actual
                filtrarUsuario( servletRequest,  servletResponse, filterChain,requestURI,  request,   response) ;
            }else{
                //La página solicitada no está en la carpeta /usuario entonces el filtro no aplica

                filterChain.doFilter(servletRequest, servletResponse);
            }
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }

    }



    public void filtrarUsuario(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain, String requestURI, HttpServletRequest request,  HttpServletResponse response) throws IOException, ServletException, ServletException, IOException {

        //Obtenemos el objeto loginBean de la sesión actual
        LoginBean userManager = (LoginBean) request.getSession().getAttribute("loginBean");

        if (userManager != null) {
            if (userManager.isAutenticado()) {
                //El usuario está logueado entonces si puede ver la página solicitada
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                //El usuario no está logueado, entonces se redirecciona al inicio
                response.sendRedirect(request.getContextPath() + PAGINA_INICIO);
            }
        } else {
            //El usuario no está logueado, entonces se redirecciona al inicio
            response.sendRedirect(request.getContextPath() + PAGINA_INICIO);
        }


    }
}
