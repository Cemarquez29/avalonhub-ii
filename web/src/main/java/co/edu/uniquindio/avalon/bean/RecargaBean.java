package co.edu.uniquindio.avalon.bean;

import co.edu.uniquindio.avalon.entidades.Recarga;
import co.edu.uniquindio.avalon.entidades.Usuario;
import co.edu.uniquindio.avalon.servicios.RecargaServicio;
import co.edu.uniquindio.avalon.servicios.UsuarioServicio;
import io.micrometer.core.annotation.Timed;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.time.LocalDate;

@Component
@ViewScoped
public class RecargaBean implements Serializable {

    @Autowired
    private transient UsuarioServicio usuarioServicio;

    @Autowired
    private transient RecargaServicio recargaServicio;

    @Value(value ="#{loginBean.usuario}")
    @Getter @Setter
    private Usuario usuario;

    @Getter @Setter
    private String username;

    @Getter @Setter
    private String usernameConfirm;

    @Getter @Setter
    private double valor;

    private final String perfilURL = "/usuario/perfil?faces-redirect=true";

    @Timed(value = "recargarsaldo.time", description = "Tiempo de respuesta de recargar saldo")
    public String realizarRecarga(){
        if(username.equals(usernameConfirm)){
            if(usuario.getUsername().equals(username)){
                if(valor>0){
                    Recarga recarga = new Recarga();
                    recarga.setFecha(LocalDate.now());
                    recarga.setUsuario(usuario);
                    recarga.setValor(valor);
                    recarga.setEstado(true);
                    recargaServicio.registrarRecarga(recarga, usuario);
                    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alerta","Recarga exitosa!");
                    FacesContext.getCurrentInstance().addMessage(null, facesMsg);
                    return "/usuario/perfil?faces-redirect=true";
                }else{
                    FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alerta","Ingrese un valor válido!");
                    FacesContext.getCurrentInstance().addMessage(null, facesMsg);
                }
            }else{
                FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alerta","Debe ingresar su propio nombre de usuario!");
                FacesContext.getCurrentInstance().addMessage(null, facesMsg);
            }

        }else{
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alerta","Ambos usuario no coinciden!");
            FacesContext.getCurrentInstance().addMessage(null, facesMsg);
        }

        return null;
    }

    public String irPerfil(){
        return perfilURL;
    }

    public String obtenerHorasStandard(){
        int horas = (int) (usuario.getSaldo() / 2000);
        return  horas + "";
    }

    public String obtenerHorasLogitech(){
        int horas = (int) (usuario.getSaldo() / 2300);
        return  horas + "";
    }
    public String obtenerHorasVip(){
        int horas = (int) (usuario.getSaldo() / 2500);
        return  horas + "";
    }

}
