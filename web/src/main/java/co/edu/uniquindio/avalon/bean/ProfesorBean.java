package co.edu.uniquindio.avalon.bean;

import co.edu.uniquindio.avalon.entidades.*;
import co.edu.uniquindio.avalon.servicios.CoachServicio;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.util.List;

@Component
@ViewScoped
public class ProfesorBean implements Serializable {

    @Value(value ="#{loginBean.usuario}")
    @Getter
    @Setter
    private Usuario usuario;

    @Getter @Setter
    private List<Coach> listaProfesores;

    @Autowired
    private transient CoachServicio coachServicio;

    @Getter @Setter
    private Especialidad especialidad;

    @Value("#{param['juego']}")
    private String juegoSeleccionado;

    @PostConstruct
    public void init(){
        if(juegoSeleccionado!=null) {
            this.listaProfesores = coachServicio.listarProfesoresPorJuego(Integer.parseInt(juegoSeleccionado));
        }
    }

    public Especialidad obtenerEspecialidad(String username)
    {
        return coachServicio.especialidadPorJuego(Integer.parseInt(juegoSeleccionado),username);
    }


    public String elegirHorario(String coach){
        return "/usuario/elegirHorario?faces-redirect=true&amp;juego=" + juegoSeleccionado + "&amp;coach=" + coach;
    }
}