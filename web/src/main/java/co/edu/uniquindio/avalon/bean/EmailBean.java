package co.edu.uniquindio.avalon.bean;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("serial")
public class EmailBean implements Serializable {


    private static final Logger LOGGER = Logger.getLogger(EmailBean.class.getName());

    private static Random rand;

    static {
        try {
            rand = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }
    //Llenar usuario y contraseña antes de ejecutar, también se deberá activar la opción de aplicaciones poco seguras de google.
    private static String usuario = "resonance.snc@gmail.com";
    private static String contrasenia =  "bceresonance.2629";
    private static String mensaje;
    private static String remitente;
    private static String asunto;
    private static String NUMEROS = "0123456789";
    private static String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";


    public static void sendEmailContraseña (String remitente, String datos) {

        //Variable for gmail
        String host="smtp.gmail.com";
        mensaje ="<table style=\"max-width: 600px; padding: 10px; margin: 0 auto; border-collapse: collapse;\">\n" +
                "    <tr>\n" +
                "      <td style=\"background-color: #ecf0f1; text-align: left; padding: 0\">\n" +
                "        <a href=\"https://github.com/Cemarquez/ResonancePlaces\">\n" +
                "          <img width=\"20%\" style=\"display:block; margin: 1.5% 3%;\" src=\"https://i.postimg.cc/4x5HWW4k/image.png\">\n" +
                "        </a>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "\n" +
                "    <tr>\n" +
                "      <td style=\"background-color: #ecf0f1\">\n" +
                "        <div style=\"color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif\">\n" +
                "          <h2 style=\"color: #ff0000; margin: 0 0 7px\">¡Recuperación de contraseña exitosa!</h2>\n" +
                "          <p style=\"margin: 2px; font-size: 15px\">\n" +
                "            Te generamos una contraseña temporal, con la cual podras ingresar a tu cuenta y poder cambiar a una contraseña de tu preferencia.\n" +
                "            <br><br>\n" +
                "            Contraseña temporal:</p>\n" +
                "          <br>\n" +
                "          <h3 style=\"color: #ff0000; margin: 0 0 7px\">"+datos+"</h3>\n" +
                "          <div style=\"width: 100%;margin:20px 0; display: inline-block;text-align: center\">\n" +
                "          </div>\n" +
                "          <div style=\"width: 100%; text-align: center\">\n" +
                "            <a style=\"text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db\" href=\"https://github.com/Cemarquez/AvalonHUB\">Ir a la página</a>\n" +
                "          </div>\n" +
                "          <p style=\"color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0\">Avalon Gaming 2021</p>\n" +
                "        </div>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "</table>";

        asunto = "Recuperación de contraseña - Avanlon Gaming";
        String from = "resonance.snc@gmail.com";
        //get the system properties
        Properties properties = System.getProperties();

        //setting important information to properties object

        //host set
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port","465");
        properties.put("mail.smtp.ssl.enable","true");
        properties.put("mail.smtp.auth","true");

        //Step 1: to get the session object..
        Session session=Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario,contrasenia);
            }



        });

        session.setDebug(true);

        //Step 2 : compose the message [text,multi media]
        MimeMessage m = new MimeMessage(session);

        try {

            //from email
            m.setFrom(from);

            //adding recipient to message
            m.addRecipient(Message.RecipientType.TO, new InternetAddress(remitente));

            //adding subject to message
            m.setSubject(asunto);


            //adding text to message
            m.setContent(mensaje, "text/html");

            //send

            //Step 3 : send the message using Transport class
            Transport.send(m);

        }catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }

    }

    public static void sendEmailContacto (String emailUser, String info, String asunto) {

        //Variable for gmail
        String host="smtp.gmail.com";
        mensaje ="<table style=\"max-width: 600px; padding: 10px; margin: 0 auto; border-collapse: collapse;\">\n" +
                "    <tr>\n" +
                "      <td style=\"background-color: #ecf0f1; text-align: left; padding: 0\">\n" +
                "        <a href=\"https://github.com/Cemarquez/ResonancePlaces\">\n" +
                "          <img width=\"20%\" style=\"display:block; margin: 1.5% 3%;\" src=\"https://i.postimg.cc/4x5HWW4k/image.png\">\n" +
                "        </a>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "\n" +
                "    <tr>\n" +
                "      <td style=\"background-color: #ecf0f1\">\n" +
                "        <div style=\"color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif\">\n" +
                "          <h2 style=\"color: #ff0000; margin: 0 0 7px\">¡SOPORTE!</h2>\n" +
                "          <p style=\"margin: 2px; font-size: 15px\">\n" +
                info + "\n" +
                "            <br><br>\n" +
                "            El correo del usuario es:</p>\n" +
                "          <br>\n" +
                "          <h3 style=\"color: #ff0000; margin: 0 0 7px\">"+ emailUser +" </h3>\n" +
                "          <div style=\"width: 100%;margin:20px 0; display: inline-block;text-align: center\">\n" +
                "          </div>\n" +
                "          <div style=\"width: 100%; text-align: center\">\n" +
                "            <a style=\"text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db\" href=\"https://github.com/Cemarquez/AvalonHUB\">Ir a la página</a>\n" +
                "          </div>\n" +
                "          <p style=\"color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0\">Avalon Gaming 2021</p>\n" +
                "        </div>\n" +
                "      </td>\n" +
                "    </tr>\n" +
                "</table>";

        asunto = "SOPORTE: " + "[" + asunto + "]";
        String from = "resonance.snc@gmail.com";
        //get the system properties
        Properties properties = System.getProperties();

        //setting important information to properties object

        //host set
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port","465");
        properties.put("mail.smtp.ssl.enable","true");
        properties.put("mail.smtp.auth","true");

        //Step 1: to get the session object..
        Session session=Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario,contrasenia);
            }



        });

        session.setDebug(true);

        //Step 2 : compose the message [text,multi media]
        MimeMessage m = new MimeMessage(session);

        try {

            //from email
            m.setFrom(from);

            //adding recipient to message
            m.addRecipient(Message.RecipientType.TO, new InternetAddress(from));

            //adding subject to message
            m.setSubject(asunto);


            //adding text to message
            m.setContent(mensaje, "text/html");

            //send

            //Step 3 : send the message using Transport class
            Transport.send(m);

        }catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }

    }

    public static String getPinNumber() {
        return getPassword(NUMEROS, 4);
    }

    public static String getPassword() {
        return getPassword(8);
    }

    public static String getPassword(int length) {
        return getPassword(NUMEROS + MAYUSCULAS + MINUSCULAS, length);
    }

    public static String getPassword(String key, int length) {
        String pswd = "";
        for (int i = 0; i < length; i++) {
            pswd+=(key.charAt((int)(rand.nextDouble() * key.length())));
        }

        return pswd;
    }
}


