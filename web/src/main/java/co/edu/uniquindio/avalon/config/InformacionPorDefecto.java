package co.edu.uniquindio.avalon.config;

import co.edu.uniquindio.avalon.entidades.*;
import co.edu.uniquindio.avalon.servicios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.time.LocalDate;

@Component
public class InformacionPorDefecto implements CommandLineRunner {

    @Autowired
    private UsuarioServicio usuarioServicio;

    @Autowired
    private RecargaServicio recargaServicio;

    @Autowired
    private JuegoServicio juegoServicio;

    @Autowired
    private CoachServicio coachServicio;

    @Autowired
    private EspecialidadServicio especialidadServicio;



    @Override
    public void run(String... args) throws Exception {
        if(usuarioServicio.listarUsuarios().isEmpty()) {
            Usuario usuario = new Usuario();
            usuario.setUsername("Cemarquez");
            usuario.setCorreo("cesar@gmail.com");
            usuario.setContrasena("cesar");
            usuario.setTelefono("3213398080");
            usuario.setSaldo(7800);

            Usuario usuario1 = new Usuario();
            usuario1.setUsername("esteban180x");
            usuario1.setCorreo("esteban@gmail.com");
            usuario1.setContrasena("esteban");
            usuario1.setTelefono("3359626455");
            usuario1.setSaldo(0);

            Usuario usuario2 = new Usuario();
            usuario2.setUsername("RyanAim");
            usuario2.setCorreo("brians.giraldos@gmail.com");
            usuario2.setContrasena("brian");
            usuario2.setTelefono("316529592");
            usuario2.setSaldo(2500);

            usuarioServicio.registrarUsuario(usuario);
            usuarioServicio.registrarUsuario(usuario1);
            usuarioServicio.registrarUsuario(usuario2);

            Recarga r1 = new Recarga();
            r1.setEstado(true);
            r1.setFecha(LocalDate.now());
            r1.setValor(5000);
            r1.setUsuario(usuario);

            Recarga r2 = new Recarga();
            r2.setEstado(true);
            r2.setFecha(LocalDate.now());
            r2.setValor(2500);
            r2.setUsuario(usuario);

            recargaServicio.registrarRecarga(r1, usuario);
            recargaServicio.registrarRecarga(r2, usuario);

            Juego juego1 = new Juego();
            juego1.setDescripcion("El juego de acción por equipos del que fue pionero cuando salió hace más de 20 años");
            juego1.setNombre("CSGO");

            Juego juego2 = new Juego();
            juego2.setDescripcion("El único objetivo de Rust es sobrevivir. Todo quiere que mueras: la vida silvestre de la isla y otros habitantes, el medio ambiente, otros sobrevivientes. Haz lo que sea necesario para que dure otra noche.");
            juego2.setNombre("Rust");

            Juego juego3 = new Juego();
            juego3.setDescripcion("Fall Guys es un juego multijugador masivo tipo party con hasta 60 jugadores online en un enfrentamiento todos contra todos que se desarrolla ronda tras ronda entre un caos creciente hasta que solo queda un único vencedor.");
            juego3.setNombre("Fall Guys");

            Juego juego4 = new Juego();
            juego4.setDescripcion("Apex Legends es el galardonado juego gratuito de acción en primera persona de Respawn Entertainment. Domina un elenco creciente de leyendas con potentes habilidades.");
            juego4.setNombre("Apex Legends");

            Juego juego5 = new Juego();
            juego5.setDescripcion("Dead by Daylight es un juego de horror de multijugador (4 contra 1) en el que un jugador representa el rol del asesino despiadado y los 4 restantes juegan como supervivientes que intentan escapar de él para evitar ser capturados y asesinados.");
            juego5.setNombre("Dead by Daylight");

            juegoServicio.registarJuego(juego1);
            juegoServicio.registarJuego(juego2);
            juegoServicio.registarJuego(juego3);
            juegoServicio.registarJuego(juego4);
            juegoServicio.registarJuego(juego5);

            Coach coach1 = new Coach();
            coach1.setCorreo("gerardo.lopez@gmail.com");
            coach1.setNombre("Gerardo Lopez");
            coach1.setTelefono("3113352647");
            coach1.setUsername("GERARLOP");
            coach1.setCedula("298456512");

            Coach coach2 = new Coach();
            coach2.setCorreo("esteban.carrancha@gmail.com");
            coach2.setNombre("Esteban Carrancha");
            coach2.setTelefono("3002581120");
            coach2.setUsername("OSAKARAIN");
            coach2.setCedula("1001345102");

            Coach coach3 = new Coach();
            coach3.setCorreo("patricia.perez@gmail.com");
            coach3.setNombre("Patricia Perez");
            coach3.setTelefono("3102856565");
            coach3.setUsername("PATI");
            coach3.setCedula("1005454812");


            coachServicio.registrarProfesor(coach1);
            coachServicio.registrarProfesor(coach2);
            coachServicio.registrarProfesor(coach3);

            Especialidad especialidad1 = new Especialidad();
            especialidad1.setNombre("PVP");
            especialidad1.setDescripcion("Dominio en los encuentros PVP con la todas las armas.");
            especialidad1.setHorasJuego(12000);
            especialidad1.setJuego(juego2);
            especialidad1.setCoach(coach1);

            Especialidad especialidad2 = new Especialidad();
            especialidad2.setNombre("ATAJOS");
            especialidad2.setDescripcion("Conocimiento y habilidad en tomar todos los posibles atajos en los mapas del juego.");
            especialidad2.setHorasJuego(12000);
            especialidad2.setJuego(juego3);
            especialidad2.setCoach(coach2);

            especialidadServicio.registrarEspecialidad(especialidad1);
            especialidadServicio.registrarEspecialidad(especialidad2);

        }


    }

}
